package sanIbra.esso.back.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

@Configuration
public class DatabaseConfiguration {

    @Bean
    InitialisateurDatabase initData(DatabaseClient databaseClient) {
        return new InitialisateurDatabase(databaseClient);
    }

    @Slf4j
    public static class InitialisateurDatabase {

        public static final String SCHEMA_SQL = "schema.sql";

        DatabaseClient databaseClient;

        public InitialisateurDatabase(DatabaseClient databaseClient) {
            this.databaseClient = databaseClient;
        }

        @EventListener(ApplicationReadyEvent.class)
        public void initDatable() {
            litFichier()
                    .flatMap(script ->
                            databaseClient.sql(script)
                                    .fetch()
                                    .all()
                                    .collectList()
                                    .doOnSuccess(_ -> log.info("L'inialisation de la base s'est deroulé avec success"))
                                    .doOnError(error -> log.error("Une erreur est survenue lors de l'inialisation de la base", error))
                                    .then())
                    .subscribe();
        }

        private Mono<String> litFichier() {
            ClassPathResource resource = new ClassPathResource(SCHEMA_SQL);
            return DataBufferUtils.read(resource, new DefaultDataBufferFactory(), 4096)
                    .map(dataBuffer -> {
                        byte[] bytes = new byte[dataBuffer.readableByteCount()];
                        dataBuffer.read(bytes);
                        DataBufferUtils.release(dataBuffer);
                        return new String(bytes, StandardCharsets.UTF_8);
                    }).reduce("", (a, b) -> a + b);
        }
    }

}
