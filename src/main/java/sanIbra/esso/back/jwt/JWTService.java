package sanIbra.esso.back.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import sanIbra.esso.back.jwt.cypher.RSAKeyService;

import java.time.ZonedDateTime;

@Service
public class JWTService {

    @Autowired
    RSAKeyService rsaKeyService;

    private Mono<Algorithm> getAlgorithm() {
        return Mono.zip(rsaKeyService.getPublicKey(), rsaKeyService.getPrivateKey())
                .map(tuple -> Algorithm.RSA256(tuple.getT1(), tuple.getT2()));
    }

    public Mono<String> buildJWT(String claims, ZonedDateTime expirationDate) {
        return getAlgorithm().map(
                algorithm ->
                        JWT.create()
                                .withPayload(claims)
                                .withExpiresAt(expirationDate.toInstant())
                                .sign(algorithm));
    }

    public Mono<Boolean> verifyJWT(String jwt) {
        return getAlgorithm().map(algorithm -> {
            try {
                algorithm.verify(new JWT().decodeJwt(jwt));
            } catch (SignatureVerificationException e) {
                return false;
            }
            return true;
        });
    }


}
