package sanIbra.esso.back.jwt.cypher;

import lombok.Builder;
import lombok.Getter;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Getter
@Builder
public class RSAKeys {

    RSAPublicKey publicKey;
    RSAPrivateKey privateKey;
}
