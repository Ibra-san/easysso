package sanIbra.esso.back.jwt.cypher;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@Table("RSA_KEYS")
public class RSAKeyEntity {

    @Id
    UUID id;

    String publicKey;

    String privateKey;
}
