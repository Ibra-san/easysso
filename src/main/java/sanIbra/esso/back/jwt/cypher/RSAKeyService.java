package sanIbra.esso.back.jwt.cypher;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Service
public class RSAKeyService {


    public static final String RSA = "RSA";

    Mono<RSAKeys> rsaKeys = Mono.empty();
    @Autowired
    RSAKeysRepository RSAKeysRepository;


    public Mono<RSAPrivateKey> getPrivateKey() {
        return getRsaKeysMono()
                .map(RSAKeys::getPrivateKey);
    }

    public Mono<RSAPublicKey> getPublicKey() {
        return getRsaKeysMono()
                .map(RSAKeys::getPublicKey);
    }

    private Mono<RSAKeys> getRsaKeysMono() {
        return rsaKeys
                .switchIfEmpty(recupererOuCreerRSAKeys());
    }

    private Mono<RSAKeys> recupererOuCreerRSAKeys() {
        return recupererRSAKeysDansBaseDeDonnee()
                .switchIfEmpty(createRSAKey()
                        .flatMap(newRsaKeys ->
                                sauvegarderRSAKeys(newRsaKeys)
                                        .then(Mono.defer(() -> {
                                                    rsaKeys = Mono.just(newRsaKeys);
                                                    return rsaKeys;
                                                })
                                        )));
    }

    private Mono<RSAKeys> recupererRSAKeysDansBaseDeDonnee() {
        return RSAKeysRepository.findAll().next()
                .map(rsaKeyEntity ->
                        RSAKeys.builder()
                                .publicKey(getPublicKeyFromBase64(rsaKeyEntity.getPublicKey()))
                                .privateKey(getPrivateKeyFromBase64(rsaKeyEntity.getPrivateKey()))
                                .build()
                );
    }


    @SneakyThrows
    private static RSAPublicKey getPublicKeyFromBase64(String base64PublicKey) {
        byte[] decodedKey = Base64.getDecoder().decode(base64PublicKey);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decodedKey);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    @SneakyThrows
    private RSAPrivateKey getPrivateKeyFromBase64(String base64PrivateKey) {
        byte[] decodedKey = Base64.getDecoder().decode(base64PrivateKey);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedKey);
        return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
    }


    private Mono<Void> sauvegarderRSAKeys(RSAKeys rsaKeys) {
        String publicKeyString = Base64.getEncoder().encodeToString(rsaKeys.getPublicKey().getEncoded());
        String privateKeyString = Base64.getEncoder().encodeToString(rsaKeys.getPrivateKey().getEncoded());
        var rsaKeyEntity = new RSAKeyEntity();
        rsaKeyEntity.setPublicKey(publicKeyString);
        rsaKeyEntity.setPrivateKey(privateKeyString);
        return RSAKeysRepository.deleteAll().then(RSAKeysRepository.save(rsaKeyEntity)).then();
    }

    private Mono<RSAKeys> createRSAKey() {
        return Mono.defer(() -> {
            KeyPairGenerator generator;
            try {
                generator = KeyPairGenerator.getInstance(RSA);
            } catch (Exception e) {
                return Mono.error(e);
            }
            generator.initialize(2048);
            KeyPair pair = generator.generateKeyPair();
            return Mono.just(RSAKeys.builder().publicKey(((RSAPublicKey) pair.getPublic()))
                    .privateKey((RSAPrivateKey) pair.getPrivate())
                    .build());
        });
    }

}
