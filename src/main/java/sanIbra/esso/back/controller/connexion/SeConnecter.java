package sanIbra.esso.back.controller.connexion;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import sanIbra.esso.back.jwt.JWTService;
import java.time.ZonedDateTime;

@Service
public class SeConnecter {

    @Autowired
    JWTService jwtService;

    public Mono<ConnexionDTO> apply(String claims) {
        String refreshToken = null;
        return jwtService.buildJWT(claims, ZonedDateTime.now().plusMinutes(5))
                .map(jwt -> new ConnexionDTO(jwt, refreshToken));
    }

}
