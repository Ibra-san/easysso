package sanIbra.esso.back.controller.connexion;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class ConnexionDTO {

    String jwt;
    String refeshToken;
}
