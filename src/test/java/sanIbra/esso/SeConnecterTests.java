package sanIbra.esso;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;
import sanIbra.esso.back.controller.connexion.SeConnecter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SeConnecterTests {

    @Autowired
    SeConnecter seConnecter;

    @Test
    void retourneUnJWTApresEtreConnecter() {
        String claims = """
                {
                     "nom": "Doe",
                     "prenom": "John",
                     "roles": ["simpleUtilisateur"]
                }
                """;

        StepVerifier.create(seConnecter.apply(claims))
                .assertNext(jwt -> {
                    assertThat(jwt).isNotNull();
                    var jwtDecode = new JWT().decodeJwt(jwt.getJwt());

                    assertThat(jwtDecode.getClaims())
                            .hasEntrySatisfying(new ConditionMapString("nom", "Doe"))
                            .hasEntrySatisfying(new ConditionMapString("prenom", "John"))
                            .hasEntrySatisfying(new ConditionMapArray("roles", List.of("simpleUtilisateur")));
                })
                .verifyComplete();
    }


    public static class ConditionMapString extends Condition<Map.Entry<String, Claim>> {
        public ConditionMapString(String key, String expectedValue) {
            super(entry -> key.equals(entry.getKey()) &&
                            expectedValue.equals(entry.getValue().asString()),
                    String.format(
                            "La valeur \"%s\" n'a pas de valeur ou n'égale pas à \"%s\". ", key, expectedValue));
        }
    }


    public static class ConditionMapArray extends Condition<Map.Entry<String, Claim>> {
        public ConditionMapArray(String key, List<String> expectedValue) {
            super(entry -> key.equals(entry.getKey())
                            && entry.getValue().asArray(String.class) != null
                            && Arrays.stream(entry.getValue().asArray(String.class))
                            .allMatch(expectedValue::contains
                            )
                    ,
                    String.format(
                            "La valeur \"%s\" n'a pas toute les valeurs de la liste %s. ", key, expectedValue));
        }
    }
}
